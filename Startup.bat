@echo off
mode con:cols=120 lines=20
color 02

set desktop="P:\Local Data\Desktop"
set startup="P:\Local Data\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup"
rem lokasi startup di remote system. sesuaikan pada konfigurasi masing2 server.


set desktop_lokal="%USERPROFILE%\Desktop"
set startup_lokal="%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup"

goto :end

:end
vol p: >nul 2>nul && vol q: >nul 2>nul
rem p & merupakan drive gamedisk yang akan di gunakan. sesuaikan dengan konfigurasi client.
rem untuk membypass agar tidak tersimpan di sistem. matikan drive q pada client.

if errorlevel 1 (
    echo Mode Lokal
	timeout 2 > NUL
) else (
    echo Mode Diskless
	
	rmdir /Q /S %desktop_lokal%
	rmdir /Q /S %startup_lokal%

	mklink /D %startup_lokal% %startup%
	mklink /D %desktop_lokal% %desktop%
	timeout 2 > NUL
	rem pause > nul
)
exit

:start

vol p: >nul 2>nul  && vol q: >nul 2>nul
rem p & merupakan drive gamedisk yang akan di gunakan. sesuaikan dengan konfigurasi client.
rem untuk membypass agar tidak tersimpan di sistem. matikan drive q pada client.

if errorlevel 1 (
    echo Mode Lokal
	timeout 2 > NUL
) else (
    echo Mode Diskless
	
	rmdir /Q /S "%USERPROFILE%\Desktop"
	rmdir /Q /S "%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup"

	mklink /D "%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup" "P:\Local Data\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup"
	mklink /D "%USERPROFILE%\Desktop" "P:\Local Data\Desktop"
	timeout 2 > NUL
	rem pause > nul
)

exit
